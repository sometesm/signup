import React from 'react'
import './Gender.css'

const Gender = (props) => {
  return <div className='Gender'>
    <div className="btn-group">

      <ul className="flex-container widget longhand cell-container">
        <li className="flex-item cell col-xs-4 col-sm-4 col-md-4 col-lg-4">
          <input id="radio1" type="radio" name="radio" onChange={() => { props.setGender("male") }} />
          <label htmlFor="radio1">MALE</label>
        </li>
        <li className="flex-item cell col-xs-4 col-sm-4 col-md-4 col-lg-4">
          <input id="radio2" type="radio" name="radio" onChange={() => { props.setGender("female") }} />
          <label htmlFor="radio2">FEMALE</label>
        </li>
        <li className="flex-item cell col-xs-4 col-sm-4 col-md-4 col-lg-4">
          <input id="radio3" type="radio" name="radio" onChange={() => { props.setGender("unspecified") }} />
          <label htmlFor="radio3">UNSPECIFIED</label>
        </li>
      </ul>

    </div>

  </div>
};

export default Gender;
