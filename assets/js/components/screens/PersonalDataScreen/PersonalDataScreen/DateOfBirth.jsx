import React from 'react'
import Aux from '../../../common/hoc/aux'
import './DateOfBirth.css'

Number.prototype.pad = function(size) {
  let s = String(this);
  while (s.length < (size || 2)) {s = "0" + s;}
  return s;
};

class DateOfBirth extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      day: "",
      month: "",
      year: "",
    };

    this.onDayChanged = this.onDayChanged.bind(this);
    this.onMonthChanged = this.onMonthChanged.bind(this);
    this.onYearChanged = this.onYearChanged.bind(this);
  }

  validateDate (year, month, day) {
    this.props.onDateSelected(year, parseInt(month), parseInt(day));
  }

  onDayChanged (day) {
    let dayInt = parseInt(day);

    if (!dayInt) {
      this.setState({day: ""});
    } else if (dayInt >= 1 && dayInt <= 31) {
      this.setState({day: dayInt.pad(2)});
    } else {
      dayInt = this.state.day;
    }
    this.validateDate(this.state.year, this.state.month, dayInt);
  }

  onMonthChanged (month) {
    let monthInt = parseInt(month);

    if (!monthInt) {
      this.setState({month: ""});
    } else if (monthInt >= 1 && monthInt <= 12) {
      this.setState({month: monthInt.pad(2)});
    } else {
      monthInt = this.state.month;
    }
    this.validateDate(this.state.year, monthInt, this.state.day);
  }

  onYearChanged (year) {
    let yearInt = parseInt(year);

    if (!yearInt) {
      this.setState({year: ""});
    } else if (yearInt >= 1 && yearInt <= 9999) {
      this.setState({year: yearInt});
    } else {
      yearInt = this.state.year;
    }
    this.validateDate(yearInt, this.state.month, this.state.day);
  }

  render () {
    let style = {display: "flex", color: "black", margin: "0"};

    return <Aux>
      <ul className="flex-outer cell-container widget" style={style}>
        <li className='cell'>
          <input
            onChange={e => { this.onDayChanged(e.target.value) }}
            value={this.state.day}
            type="text"
            placeholder="DD"
            id="day"
            className='center' />
        </li>
        <li className='cell'>
          <input
            onChange={e => { this.onMonthChanged(e.target.value) }}
            value={this.state.month}
            type="text"
            placeholder="MM"
            id="month"
            className='center' />
        </li>
        <li className='cell'>
          <input
            onChange={e => { this.onYearChanged(e.target.value) }}
            value={this.state.year}
            type="text"
            placeholder="YYYY"
            id="year"
            className='center' />
        </li>
      </ul>

    </Aux>
  }
}

export default DateOfBirth;
