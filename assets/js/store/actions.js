export const SET_EMAIL = 'SET_EMAIL';
export const SET_PASSWORD = 'SET_PASSWORD';
export const SET_DATE_OF_BIRTH = 'SET_DATE_OF_BIRTH';
export const SET_GENDER = 'SET_GENDER';
export const SET_HOW_HEAR_ABOUT_US = 'SET_HOW_HEAR_ABOUT_US';
