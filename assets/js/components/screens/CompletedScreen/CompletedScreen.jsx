import React from 'react'

import Header from '../../common/Header'
import Success from './Success'

import classes from './CompletedScreen.css'
import * as actionTypes from "../../../store/actions";
import {connect} from "react-redux";


class CompletedScreen extends React.Component {
  constructor (props) {
    super(props);

    this.showReduxContent = this.showReduxContent.bind(this);
  }

  showReduxContent () {
    console.log({
      user_data: {
        email: this.props.redux.email,
        password: this.props.redux.password,
        date_of_birth: this.props.redux.date_of_birth,
        gender: this.props.redux.gender,
        how_hear_about_us: this.props.redux.how_hear_about_us,
      }
    });
  }

  render () {
    return <div className='CompletedScreen'>
      <Header title="Thank you!" step={3} />

      <Success />
      <button
        className='Button'
        onClick={this.showReduxContent}
      >Go to Dashboard -></button>

    </div>
  }
}


const mapStateToProps = state => {
  return {
    redux: {
      email: state.email,
      password: state.password,
      date_of_birth: state.date_of_birth,
      gender: state.gender,
      how_hear_about_us: state.how_hear_about_us,
    }
  }
};

export default connect(mapStateToProps)(CompletedScreen);
