import React from 'react'
import Header from '../../common/Header'
import Footer from '../../common/Footer'

import DateOfBirth from './PersonalDataScreen/DateOfBirth'
import Gender from './PersonalDataScreen/Gender'
import WhereDidYouHearAboutUs from './PersonalDataScreen/WhereDidYouHearAboutUs'
import Error from '../../common/Error'

import './PersonalDataScreen.css'
import * as actionTypes from "../../../store/actions";
import {connect} from "react-redux";

const validateIsDateExists = (dateDict, dateObj) => {
  return ( dateDict.year === dateObj.getFullYear() && (dateDict.month - 1) === dateObj.getMonth() );
};

const validate18plus = (dateObj) => {
  const diff = new Date( new Date().getTime() - dateObj.getTime() );
  return ! ( (diff.getUTCFullYear() - 1970) < 18 );
};

class PersonalDataScreen extends React.Component {
  constructor (props) {
    super(props);

    this.state = {
      DateOfBirthErr: null,
      GenderErr: null
    };

    this.dateOfBirth = { year: null, month: null, date: null };
    this.dateOfBirthDateObj = null;
    this.gender = null;
    this.how_hear_about_us = null;

    this.validateDateOfBirth = this.validateDateOfBirth.bind(this);
    this.setDateOfBirth = this.setDateOfBirth.bind(this);
    this.setGender = this.setGender.bind(this);
    this.setWhereFound = this.setWhereFound.bind(this);
    this.validateForm = this.validateForm.bind(this);
  }

  setDateOfBirth (year, month, date) {
    this.dateOfBirth = {
      year: year?year:null,
      month: month?month:null,
      date: date?date:null };

    this.dateOfBirthDateObj = new Date(year, month - 1, date);
  }

  setGender (gender) {
    this.gender = gender;
  }

  setWhereFound (how_hear_about_us) {
    if (!how_hear_about_us) {
      how_hear_about_us = null;
    }
    this.how_hear_about_us = how_hear_about_us;
  }

  validateDateOfBirth () {
    let isValid = false;

    const date = this.dateOfBirth;
    if (date.year && date.month && date.date) {

      do {
        if ( ! validate18plus(this.dateOfBirthDateObj) ) {
          this.setState({DateOfBirthErr: '18+ only!'});
          break;
        }

        if ( ! validateIsDateExists( this.dateOfBirth, this.dateOfBirthDateObj ) ) {
          this.setState({DateOfBirthErr: "the date does't exist!"});
          break;
        }
        isValid = true;
        this.setState({DateOfBirthErr: null});
      } while (false);
    } else {
      this.setState({DateOfBirthErr: `please choose the date!`});
    }

    return isValid;
  }

  validateGender () {
    let isValid = true;

    if ( ! this.gender ) {
      this.setState({GenderErr: `please select any gender!`});
      isValid = false;
    } else {
      this.setState({GenderErr: null});
    }
    return isValid;
  }

  validateForm () {
    const dateOfBirthValid = this.validateDateOfBirth();
    const genderValid = this.validateGender();

    if ( dateOfBirthValid && genderValid ) {
      this.props.setGlobalDateOfBirth( this.dateOfBirthDateObj.getTime() );
      this.props.setGlobalGender(this.gender);
      this.props.setGlobalWhereFound(this.how_hear_about_us);

      this.props.next();
    }
  }

  render () {
    return <div className="PersonalDataScreen">
      <Header title="Signup" step={2} />

      <div className="container" style={{marginTop: "70px"}}>

        <div className='uppercaseFont'>DATE OF BIRTH</div>
        <Error>{this.state.DateOfBirthErr}</Error>
        <DateOfBirth onDateSelected={this.setDateOfBirth}/>

        <div className='uppercaseFont' style={{marginTop: "30px"}}>GENDER</div>
        <Error>{this.state.GenderErr}</Error>
        <Gender setGender={this.setGender} />

        <div className='uppercaseFont' style={{marginTop: "50px"}}>WHERE DID YOU HEAR ABOUT US</div>
        <WhereDidYouHearAboutUs setWhereFound={this.setWhereFound}/>
      </div>

      <Footer prev={this.props.prev} next={this.validateForm} />
    </div>
  }
}

const mapStateToProps = state => {
  return {
    date_of_birth: state.date_of_birth,
    gender: state.gender,
    how_hear_about_us: state.how_hear_about_us,
  }
};

const mapDispatchToProps = dispatch => {
  return {
    setGlobalDateOfBirth: (date) => dispatch({
      type: actionTypes.SET_DATE_OF_BIRTH, 
      date_of_birth: date}),
    
    setGlobalGender: (gender) => dispatch({
      type: actionTypes.SET_GENDER, 
      gender: gender}),
    
    setGlobalWhereFound: (how_hear_about_us) => dispatch({
      type: actionTypes.SET_HOW_HEAR_ABOUT_US, 
      how_hear_about_us: how_hear_about_us}),
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(PersonalDataScreen);
