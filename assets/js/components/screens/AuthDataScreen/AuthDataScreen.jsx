import React from 'react'
import Header from '../../common/Header'
import Footer from '../../common/Footer'

import './AuthDataScreen.css'
import Aux from "../../common/hoc/aux";
import Container from "../../common/Container"
import Error from "../../common/Error"

import PropTypes from 'prop-types';
import {connect} from "react-redux";

import * as actionTypes from '../../../store/actions'


class AuthDataScreen extends React.Component {
  constructor (props) {
    super(props);

    this.onEmailChanged = this.onEmailChanged.bind(this);
    this.onPass1Changed = this.onPass1Changed.bind(this);
    this.onPass2Changed = this.onPass2Changed.bind(this);
    this.validate = this.validate.bind(this);
    this.validateEmail = this.validateEmail.bind(this);
    this.validatePasswords = this.validatePasswords.bind(this);

    this.state = {
      email: props.email,
      pass1: "",
      pass2: "",

      emailErr: null,
      passErr: null,
    };
  }

  validateEmail () {
    const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    const valid = re.test(this.state.email);

    if ( ! valid ) {
      this.setState({emailErr: "email is not valid!"});
    } else {
      this.setState({emailErr: null});
    }
    return valid;
  }

  validatePasswords () {
    let valid;

    do {
      if (!this.state.pass1) { this.setState({passErr: "password should not be empty!"}); break; }
      if (this.state.pass1 !== this.state.pass2) { this.setState({passErr: "passwords are not equal!"}); break; }
      if (this.state.pass1.length < 6) { this.setState({passErr: "password should contains 6 or more symbols!"}); break; }

      valid = true;
      this.setState({passErr: null});
    } while (false);

    return valid;
  }

  validate () {
    const emailValid = this.validateEmail();
    const passValid = this.validatePasswords();

    if ( emailValid && passValid ) {
      this.props.setGlobalEmail(this.state.email);
      this.props.setGlobalPassword(this.state.pass1);
      this.props.next();
    }
  }

  onEmailChanged (e) {
    this.setState({
      email: e.target.value
    });
  }

  onPass1Changed (e) {
    this.setState({
      pass1: e.target.value
    });
  }

  onPass2Changed (e) {
    this.setState({
      pass2: e.target.value
    });
  }

  render () {
    return (
      <Aux>
        <Container>
          <Header title="Signup" step={1} />

          <div className="container" style={{marginTop: "90px"}}>
            <form>
              <ul className="flex-outer">
                <li className="input">
                  <label htmlFor="email" className='uppercaseFont' style={{color: "red"}}>EMAIL IS REQUIRED</label>
                  <Error>{this.state.emailErr}</Error>
                  <input type="email" id="email" value={this.state.email} onChange={this.onEmailChanged} />
                </li>
                <li className="input">
                  <label htmlFor="password" className='uppercaseFont'>PASSWORD</label>
                  <Error>{this.state.passErr}</Error>
                  <input type="password" id="password" onChange={this.onPass1Changed} />
                </li>
                <li className="input">
                  <label htmlFor="password-confirm" className='uppercaseFont'>CONFIRM PASSWORD</label>
                  <input type="password" id="password-confirm" onChange={this.onPass2Changed} />
                </li>
              </ul>
            </form>
          </div>

          <Footer next={this.validate} />
        </Container>
      </Aux>
    );
  }
}

AuthDataScreen.propTypes = {
  next: PropTypes.func
};

const mapStateToProps = state => {
  return {
    email: state.email,
    password: state.password,
  }
};

const mapDispatchToProps = dispatch => {
  return {
    setGlobalEmail: (email) => dispatch({type: actionTypes.SET_EMAIL, email: email}),
    setGlobalPassword: (password) => dispatch({type: actionTypes.SET_PASSWORD, password: password}),
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(AuthDataScreen);
