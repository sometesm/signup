import React from 'react'

const ProgressBar = (props) => {
  let percents = props.percents;

  return (
    <div className="w3-light-grey w3-tiny" style={{borderTop: "solid 1px #999", borderBottom: "solid 1px #999", marginTop: "10px"}}>
      <div className="w3-container w3-blue w3-center" style={{width:`${percents}%`}}>&nbsp;</div>
    </div>
  );
};

export default ProgressBar;
