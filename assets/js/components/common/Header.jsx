import React from 'react'
import ProgressBar from './ProgressBar'

const Header = (props) => {
  let style = {
    paddingTop: "10px",
    textAlign: "center",
    fontSize: "1.5em",
    color: "#0078ff",
  };

  const steps = {
    1: 33,
    2: 66,
    3: 100,
  };

  return (
    <div style={style}>
      <span style={{fontFamily: "'Open Sans', sans-serif"}}>{props.title}</span>
      <ProgressBar percents={ steps[props.step] }/>
    </div>
  );
};

export default Header;
