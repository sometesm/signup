import * as actionTypes from './actions'

const initialState = {
  email: "",
  password: "",
  date_of_birth: "",
  gender: "",
  how_hear_about_us: "",
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.SET_EMAIL:
      return {
        ...state,
        email: action.email
      };
    case actionTypes.SET_PASSWORD:
      return {
        ...state,
        password: action.password
      };
    case actionTypes.SET_DATE_OF_BIRTH:
      return {
        ...state,
        date_of_birth: action.date_of_birth
      };
    case actionTypes.SET_GENDER:
      return {
        ...state,
        gender: action.gender
      };
    case actionTypes.SET_HOW_HEAR_ABOUT_US:
      return {
        ...state,
        how_hear_about_us: action.how_hear_about_us
      };
  }

  return state;
};

export default reducer;
