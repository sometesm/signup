import React from 'react'

const Container = (props) => {
  return (
    <div style={{position: "relative", height: "100%"}}>
      {props.children}
    </div>
  );
};

export default Container;
