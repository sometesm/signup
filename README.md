Execute once:

    python3 -m virtualenv venv
    source ./venv/bin/activate
    pip install -r requirements.txt
    
    npm install

For each use:

    python manage.py runserver 127.0.0.1:8000
    npm run watch
