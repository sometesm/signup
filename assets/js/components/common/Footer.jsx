import React from 'react'

const Footer = (props) => {
  let content = {
    back: null,
    next: null,
  };

  if (props.prev) {
    content.back = <span
      style={{bottom: "20px", position: "absolute", left: "20px", fontFamily: "'Bai Jamjuree', sans-serif", cursor: "pointer", color: "#999"}}
      onClick={ () => { props.prev() } }
    >Back</span>
  }

  if (props.next) {
    content.next = <span
      style={{bottom: "20px", position: "absolute", right: "20px", fontFamily: "'Bai Jamjuree', sans-serif", cursor: "pointer", color: "rgb(0, 120, 255)"}}
      onClick={ () => { props.next() } }
    >Next -></span>;
  }

  return (
    <div>
      {content.back}
      {content.next}
    </div>
  );
};

export default Footer;
