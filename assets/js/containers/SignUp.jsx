import React from 'react'

import AuthDataScreen from '../components/screens/AuthDataScreen/AuthDataScreen';
import PersonalDataScreen from '../components/screens/PersonalDataScreen/PersonalDataScreen';
import CompletedScreen from '../components/screens/CompletedScreen/CompletedScreen';

import './SignUp.css'

import * as actionTypes from './../store/actions'


import { connect } from 'react-redux'


class SignUpForm extends React.Component {
  constructor (props) {
    super(props);

    this.state = {
      screen: 0
    };

    this.goNextScreen = this.goNextScreen.bind(this);
    this.goPrevScreen = this.goPrevScreen.bind(this);

    this.screens = [
      <AuthDataScreen
        next={this.goNextScreen} />,

      <PersonalDataScreen
        next={this.goNextScreen}
        prev={this.goPrevScreen} />,

      <CompletedScreen />,
    ];
  }

  goNextScreen() {
    if ( this.state.screen < this.screens.length ) {
      this.setState({screen: this.state.screen + 1});
    }
  }

  goPrevScreen() {
    if ( this.state.screen > 0 ) {
      this.setState({screen: this.state.screen - 1});
    }
  }

  render () {
    return (
      <div className="body">
        <div className="mainScreen">
          { this.screens[this.state.screen] }
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    screen: state.screen
  }
};

const mapDispatchToProps = dispatch => {
  return {
    onNextScreen: () => dispatch({type: actionTypes.SET_DATE_OF_BIRTH}),
    onPrevScreen: () => dispatch({type: 'GO_PREV_SCREEN'}),
  }
};


export default connect(mapStateToProps, mapDispatchToProps)(SignUpForm);
