import React from 'react'
import './WhereDidYouHearAboutUs.css'


const WhereDidYouHearAboutUs = (props) => {
  return <select className="widget" name='options' onChange={(e) => { props.setWhereFound(e.target.value) }}>
    <option value={null}>
    </option>
    <option value='friend'>
      From my friend
    </option>
    <option value='google'>
      Google
    </option>
    <option value='facebook'>
      Facebook
    </option>
  </select>
};

export default WhereDidYouHearAboutUs;
