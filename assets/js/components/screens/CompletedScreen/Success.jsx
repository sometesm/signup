import React from 'react'

import classes from './Success.css'

const Success = (props) => {
  return <svg className="checkmark" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 52 52">
      <circle className="checkmark__circle" cx="300" cy="300" r="300" fill="none"/>
      <path className="checkmark__check" fill="none" d="M14.1 27.2l7.1 7.2 16.7-16.8"/>
    </svg>
};

export default Success;
