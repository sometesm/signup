import React from 'react'

import './Error.css'

const Error = (props) => {
  return (
    <div className="Error">
      {props.children ? <div className="Body">{props.children}</div> : null }
    </div>
  );
};

export default Error;
